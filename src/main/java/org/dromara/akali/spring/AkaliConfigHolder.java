package org.dromara.akali.spring;

import org.dromara.akali.config.AkaliProperty;

public class AkaliConfigHolder {

    private static AkaliProperty property;

    public static void setProperty(AkaliProperty property){
        AkaliConfigHolder.property = property;
    }

    public static AkaliProperty loadProperty(){
        return property;
    }
}
