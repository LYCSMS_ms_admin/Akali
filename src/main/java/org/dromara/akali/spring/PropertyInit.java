package org.dromara.akali.spring;

import org.dromara.akali.config.AkaliProperty;
import org.springframework.beans.factory.InitializingBean;

public class PropertyInit implements InitializingBean {

    private final AkaliProperty akaliProperty;

    public PropertyInit(AkaliProperty akaliProperty) {
        this.akaliProperty = akaliProperty;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        AkaliConfigHolder.setProperty(akaliProperty);
    }
}
